#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of passerelle-atreal-openads - a Publik connector to openADS
#
# Copyright (C) 2019 Atreal
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Testing views."""

from django.urls.base import resolve

from atreal_openads.views import (
    get_connecteur_from_request,
    get_collectivite_from_request,
    AtrealOpenadsView,
    ForwardFileView,
    ForwardFileListView,
    ForwardFileUpdateView,
    ForwardFileDeleteView,
    CollectiviteView,
    CollectiviteListView,
    CollectiviteCreateView,
    CollectiviteUpdateView,
    CollectiviteDeleteView,
    GuichetView,
    GuichetCreateView,
    GuichetUpdateView,
    GuichetDeleteView
)

from atreal_openads.models import Collectivite


# pylint: disable=unused-argument,redefined-outer-name,invalid-name
def test_get_connecteur_from_request(atreal_openads, forwardfile_1, request_1):
    """Test for function 'get_connecteur_from_request()'."""

    request_1.path = '/manage/atreal-openads/%s/forward-file/%s' % (
        atreal_openads.slug, forwardfile_1.id)
    request_1.resolver_match = resolve(request_1.path)

    view = ForwardFileView()
    view.request = request_1

    connecteur = get_connecteur_from_request(view)
    assert connecteur is not None
    assert connecteur.slug == atreal_openads.slug


# pylint: disable=unused-argument,redefined-outer-name
def test_get_collectivite_from_request(atreal_openads, collectivite_1, request_1):  # noqa: E501, pylint: disable=invalid-name
    """Test for function 'get_collectivite_from_request()'."""

    request_1.path = '/manage/atreal-openads/%s/collectivite/%s/forward-files' % (
        atreal_openads.slug, collectivite_1.id)
    request_1.resolver_match = resolve(request_1.path)

    view = ForwardFileListView()
    view.request = request_1

    collectivite = get_collectivite_from_request(view)
    assert collectivite is not None
    assert collectivite.id == collectivite_1.id


# pylint: disable=too-many-statements
def test_forwardfile_view(atreal_openads, collectivite_1, forwardfile_1, request_1):
    """Test for views 'ForwardFile*View'."""

    request_1.path = '/manage/atreal-openads/%s/forward-file/%s' % (
        atreal_openads.slug, forwardfile_1.id)
    request_1.resolver_match = resolve(request_1.path)

    view = ForwardFileView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug

    view = ForwardFileUpdateView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/forward-file/%s' % (
        atreal_openads.slug, forwardfile_1.id)
    request_1.GET['back-to'] = 'list-forward-files'
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/forward-files' % atreal_openads.slug
    request_1.GET['back-to'] = 'col-list-forward-files'
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/collectivite/%s/forward-files' % (
        atreal_openads.slug, collectivite_1.id)

    view = ForwardFileDeleteView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    del request_1.GET['back-to']
    url = view.get_success_url()
    assert url == u'/atreal-openads/%s/' % atreal_openads.slug
    request_1.GET['back-to'] = 'list-forward-files'
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/forward-files' % atreal_openads.slug
    request_1.GET['back-to'] = 'col-list-forward-files'
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/collectivite/%s/forward-files' % (
        atreal_openads.slug, collectivite_1.id)

    request_1.path = '/manage/atreal-openads/%s/collectivite/%s/forward-files' % (
        atreal_openads.slug, collectivite_1.id)
    request_1.resolver_match = resolve(request_1.path)
    view = ForwardFileListView()
    view.request = request_1
    view.object_list = []
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    assert context['collectivite'].id == collectivite_1.id

    queryset = view.get_queryset()
    assert queryset.query is not None
    assert list(queryset.query.order_by) == ['id']
    assert queryset.query.default_ordering
    assert queryset.query.get_meta().ordering == ['-last_update_datetime']
    assert queryset.ordered

    request_1.GET['order-by'] = '-id'
    queryset = view.get_queryset()
    assert queryset.query is not None
    assert list(queryset.query.order_by) == ['-id']
    assert queryset.query.default_ordering

    request_1.path = '/manage/atreal-openads/%s/forward-files' % atreal_openads.slug
    request_1.resolver_match = resolve(request_1.path)
    del request_1.GET['back-to']
    del request_1.GET['order-by']
    view = ForwardFileListView()
    view.request = request_1
    view.object_list = []
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug

    queryset = view.get_queryset()
    assert queryset.query is not None
    assert list(queryset.query.order_by) == ['id']
    assert queryset.query.default_ordering
    assert queryset.query.get_meta().ordering == ['-last_update_datetime']
    assert queryset.ordered


# pylint: disable=too-many-statements
def test_collectivite_view(atreal_openads, collectivite_1, forwardfile_1, request_1):
    """Test for views 'Collectivite*View'."""

    request_1.path = '/manage/atreal-openads/%s/collectivite/%s' % (
        atreal_openads.slug, collectivite_1.id)
    request_1.resolver_match = resolve(request_1.path)

    view = CollectiviteView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    assert context['guichet_add_url'] == (
        u'/manage/atreal-openads/%s/collectivite/%s/create-guichet' % (
            atreal_openads.slug, collectivite_1.id))
    assert context['forward_files_list_url'] == (
        u'/manage/atreal-openads/%s/collectivite/%s/forward-files' % (
            atreal_openads.slug, collectivite_1.id))

    view = CollectiviteUpdateView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/collectivite/%s' % (
        atreal_openads.slug, collectivite_1.id)
    request_1.GET['back-to'] = 'list-collectivites'
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/collectivites' % atreal_openads.slug

    view = CollectiviteDeleteView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    del request_1.GET['back-to']
    url = view.get_success_url()
    assert url == u'/atreal-openads/%s/' % atreal_openads.slug
    request_1.GET['back-to'] = 'list-collectivites'
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/collectivites' % atreal_openads.slug

    view = CollectiviteCreateView()
    request_1.path = '/manage/atreal-openads/%s/create-collectivite' % atreal_openads.slug
    request_1.resolver_match = resolve(request_1.path)
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    kwargs = view.get_form_kwargs()
    assert kwargs['connecteur'].slug == atreal_openads.slug
    del request_1.GET['back-to']
    url = view.get_success_url()
    assert url == u'/atreal-openads/%s/' % atreal_openads.slug
    request_1.GET['back-to'] = 'list-collectivites'
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/collectivites' % atreal_openads.slug

    request_1.path = '/manage/atreal-openads/%s/collectivites' % atreal_openads.slug
    request_1.resolver_match = resolve(request_1.path)
    view = CollectiviteListView()
    view.request = request_1
    view.object_list = []
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    assert context['collectivite_add_url'] == (
        u'/manage/atreal-openads/%s/create-collectivite' % atreal_openads.slug)

    queryset = view.get_queryset()
    assert queryset.query is not None
    assert list(queryset.query.order_by) == ['id']
    assert queryset.query.default_ordering
    assert queryset.query.get_meta().ordering == ['name']
    assert queryset.ordered

    request_1.GET['order-by'] = '-id'
    queryset = view.get_queryset()
    assert queryset.query is not None
    assert list(queryset.query.order_by) == ['-id']
    assert queryset.query.default_ordering


def test_guichet_view(atreal_openads, collectivite_1, collectivite_1_guichet, request_1):
    """Test for views 'Guichet*View'."""

    request_1.path = '/manage/atreal-openads/%s/collectivite/%s/guichet/%s' % (
        atreal_openads.slug, collectivite_1.id, collectivite_1_guichet.id)
    request_1.resolver_match = resolve(request_1.path)

    view = GuichetView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    assert context['collectivite'].id == collectivite_1.id

    view = GuichetUpdateView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    assert context['collectivite'].id == collectivite_1.id

    view = GuichetDeleteView()
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    assert context['collectivite'].id == collectivite_1.id
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/collectivite/%s' % (
        atreal_openads.slug, collectivite_1.id)

    view = GuichetCreateView()
    request_1.path = '/manage/atreal-openads/%s/collectivite/%s/create-guichet' % (
        atreal_openads.slug, collectivite_1.id)
    request_1.resolver_match = resolve(request_1.path)
    view.request = request_1
    view.object = None
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['connecteur'].slug == atreal_openads.slug
    assert context['collectivite'].id == collectivite_1.id
    kwargs = view.get_form_kwargs()
    assert kwargs['collectivite'].id == collectivite_1.id
    url = view.get_success_url()
    assert url == u'/manage/atreal-openads/%s/collectivite/%s' % (
        atreal_openads.slug, collectivite_1.id)


def test_connecteur_view(atreal_openads, request_1):
    """Test for views 'AtrealOpenadsView'."""

    request_1.path = '/atreal-openads/%s/' % atreal_openads.slug
    request_1.resolver_match = resolve(request_1.path)

    view = AtrealOpenadsView()
    view.request = request_1
    view.object = atreal_openads
    view.kwargs = request_1.resolver_match.kwargs
    context = view.get_context_data()
    assert context['collectivite_fields'] == Collectivite.get_fields()
    assert context['collectivite_add_url'] == (
        u'/manage/atreal-openads/%s/create-collectivite' % atreal_openads.slug)
