#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of passerelle-atreal-openads - a Publik connector to openADS
#
# Copyright (C) 2019 Atreal
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Testing forms."""

import datetime

from atreal_openads.forms import (
    ForwardFileForm,
    CollectiviteForm,
    GuichetForm
)

from atreal_openads.models import (
    Guichet,
    Collectivite,
)


# pylint: disable=unused-argument,redefined-outer-name
def test_forwardfile_form(forwardfile_2, atreal_openads, collectivite_1):
    """Test for ForwardFileForm."""

    form = ForwardFileForm()
    assert form.instance is not None

    form_with_instance = ForwardFileForm(instance=forwardfile_2, collectivite=collectivite_1)
    assert form_with_instance.instance is forwardfile_2
    assert form_with_instance.instance.collectivite is collectivite_1

    form_with_instance = ForwardFileForm(instance=forwardfile_2, connecteur=atreal_openads)
    assert form_with_instance.instance is forwardfile_2
    assert form_with_instance.instance.connecteur is atreal_openads

    # TODO check the queryset of the collectivite


# pylint: disable=unused-argument,redefined-outer-name
def test_collectivite_form(atreal_openads):
    """Test for CollectiviteForm."""

    form = CollectiviteForm()
    assert form.instance is not None

    col = Collectivite(
        connecteur=None,
        name=u'Ma collectivité',
        openADS_id=3
    )

    form_with_instance = CollectiviteForm(instance=col, connecteur=atreal_openads)
    assert form_with_instance.instance is col
    assert form_with_instance.instance.connecteur is atreal_openads


# pylint: disable=unused-argument,redefined-outer-name
def test_guichet_form(atreal_openads, collectivite_1):
    """Test for GuichetForm."""

    form = GuichetForm()
    assert form.instance is not None

    gui = Guichet(
        collectivite=None,
        ouverture_jour_h=datetime.time(9, 0, 0),
        fermeture_jour_h=datetime.time(18, 0, 0),
        ouverture_sem_d=1,
        fermeture_sem_d=5,
        ouverture_sem_h=datetime.time(10, 30, 0),
        fermeture_sem_h=datetime.time(12, 15, 0)
    )

    form_with_instance = GuichetForm(instance=gui, collectivite=collectivite_1)
    assert form_with_instance.instance is gui
    assert form_with_instance.instance.collectivite is collectivite_1
