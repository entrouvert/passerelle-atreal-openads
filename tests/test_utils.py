#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of passerelle-atreal-openads - a Publik connector to openADS
#
# Copyright (C) 2019 Atreal
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Testing utilities functions."""

import base64
import re

from django.utils.encoding import force_text
import pytest

from atreal_openads.utils import (
    to_dash_case,
    force_encoded_string_output,
    strip_tags,
    clean_spaces,
    normalize,
    get_file_data,
    get_file_digest,
    get_upload_path,
    get_file_extension,
    trunc_str_values,
    DictDumper
)


def test_to_dash_case():
    """Test for function 'to_dash_case()'."""

    astring = 'ACamelCaseName'
    assert to_dash_case(astring) == 'a-camel-case-name'

    assert to_dash_case('') == ''


def test_force_encoded_string_output():  # pylint: disable=invalid-name
    """Test for function 'force_encoded_string_output()'."""

    def a_str_function():
        """Return a hardcoded string 'toto'."""
        return str('toto')
    ret = force_encoded_string_output(a_str_function)()
    assert isinstance(ret, str)
    ret = force_encoded_string_output(a_str_function, 'latin1')()
    assert isinstance(ret, str)

    def an_unicode_function():
        """Return a hardcoded string 'toto' in unicode."""
        return u'toto'
    ret = force_encoded_string_output(an_unicode_function)()
    assert isinstance(ret, str)
    ret = force_encoded_string_output(an_unicode_function, 'latin1')()
    assert isinstance(ret, str)


def test_strip_tags():
    """Test for function 'strip_tags()'."""

    base_string = 'aaa b  cc '
    assert strip_tags(base_string) == base_string

    astring = base_string + '<em>dd'
    assert strip_tags(astring) == base_string + 'dd'

    astring = base_string + '<em>dd</em>'
    assert strip_tags(astring) == base_string + 'dd'

    astring = base_string + '<em>dd</em>'
    assert strip_tags(astring) == base_string + 'dd'

    astring = base_string + ' 1 < 3'
    assert strip_tags(astring) == base_string + ' 1 < 3'


def test_clean_spaces():
    """Test for function 'clean_spaces()'."""

    astring = 'aaa b  cc '
    assert clean_spaces(astring) == 'aaa b cc'

    astring = 'a\ta b\nb  c\rc  d\\n\\r\\td'
    assert clean_spaces(astring) == 'a a b b c c d d'


def test_normalize():
    """Test for function 'normalize()'."""

    assert normalize(None) == ''

    astring = 'aaa b  cc '
    assert normalize(astring) == 'aaa b cc'

    astring = 'a\ta b\nb  c\rc  d\\n\\r\\td'
    assert normalize(astring) == 'a a b b c c d d'


def test_get_file_data(fake_conf):
    """Test for function 'get_file_data()'."""

    assert get_file_data(fake_conf['TEST_FILE_CERFA_DIA']) == base64.b64encode(
        open(fake_conf['TEST_FILE_CERFA_DIA'], 'rb').read())
    assert get_file_data(fake_conf['TEST_FILE_CERFA_DIA'], b64=False) == open(
        fake_conf['TEST_FILE_CERFA_DIA'], 'rb').read()


def test_get_file_digest(fake_conf):
    """Test for function 'get_file_digest()'."""

    with open(fake_conf['TEST_FILE_CERFA_DIA'], 'rb') as file_pt:
        assert get_file_digest(file_pt) == ('cc90a620982760fdee16a5b4fe1b5ac3'
                                            'b4fe868fd02d2f70b27f1e46d283ea51')


def test_get_upload_path(forwardfile_2):
    """Test for function 'get_upload_path()'."""

    regex = r"^to_openADS__%s__%s\.pdf$" % (
        '[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}h[0-9]{2}m[0-9]{2}s[0-9]+', 'ffdf')
    assert re.search(regex, get_upload_path(forwardfile_2))


def test_get_file_extension():
    """Test for function 'get_file_extension()'."""

    assert get_file_extension('afile.pdf') == '.pdf'
    assert get_file_extension('afile', 'application/pdf') == '.pdf'
    assert get_file_extension('') == ''
    assert get_file_extension('afile') == ''


def test_trunc_str_values():
    """Test for function 'trunc_str_values()'."""

    dic = {}
    assert trunc_str_values(dic, 10) == dic
    dic = {'a': '123456789'}
    assert trunc_str_values(dic, 0) == {'a': u'…'}
    dic = {'a': '123456789'}
    assert trunc_str_values(dic, 1) == {'a': u'1…'}
    dic = {'a': '123456789'}
    assert trunc_str_values(dic, 2) == {'a': u'12…'}
    dic = {'a': '123456789'}
    assert trunc_str_values(dic, 5) == {'a': u'12345…'}
    dic = {'a': '123456789'}
    assert trunc_str_values(dic, 8) == {'a': u'12345678…'}
    dic = {'a': '123456789'}
    assert trunc_str_values(dic, 9) == {'a': u'123456789'}
    dic = {'a': '123456789'}
    assert trunc_str_values(dic, 10) == dic

    dic = {'a': '123456789', 'b123456789': '987654321'}
    assert trunc_str_values(dic, 5) == {'a': u'12345…', 'b123456789': u'98765…'}

    dic = {'a': '123456789', 'b123456789': '987654321', 'c': {'c1': 'ABCDEFGHIJK'}}
    assert trunc_str_values(dic, 5) == {'a': u'12345…', 'b123456789': u'98765…',
                                        'c': {'c1': u'ABCDE…'}}

    dic = {'a': '123456789', 'b123456789': '987654321', 'c': {'c1': 'ABCDEFGHIJK'},
           'd': ['123456789']}
    assert trunc_str_values(dic, 5) == {'a': u'12345…', 'b123456789': u'98765…',
                                        'c': {'c1': u'ABCDE…'}, 'd': [u'12345…']}

    dic = {'a': '123456789', 'b123456789': '987654321', 'c': {'c1': 'ABCDEFGHIJK'},
           'd': ['123456789', {'eeeeeeeeee': '132456789'}]}
    assert trunc_str_values(dic, 5) == {'a': u'12345…', 'b123456789': u'98765…',
                                        'c': {'c1': u'ABCDE…'},
                                        'd': [u'12345…', {'eeeeeeeeee': u'13245…'}]}


def test_dict_dumper():
    """Test for methods of class 'DictDumper'."""

    dic = {}

    dumped = DictDumper(dic, use_json_dumps=False)
    assert repr(dumped) == (u'DictDumper(dic=%r,max_str_len=%r,use_json_dumps=%r)' % (
        dic, dumped.max_str_len, dumped.use_json_dumps))
    assert str(dumped) == '{}'
    assert force_text(dumped) == u'{}'

    assert dic == dumped.dic
    assert force_text(dic) == force_text(dumped)
    dumped = DictDumper(dic, 0, use_json_dumps=False)
    assert dic == dumped.dic
    assert force_text(dic) == force_text(dumped)

    dic = {'a': '123456789'}
    dumped = DictDumper(dic, 10, use_json_dumps=False)
    assert dic == dumped.dic
    assert force_text(dic) == force_text(dumped)
    dumped = DictDumper(dic, 5, use_json_dumps=False)
    assert dic == dumped.dic
    assert force_text(dumped) == force_text({'a': u'12345…'})
    dumped = DictDumper(dic, 5, use_json_dumps=True)
    assert dic == dumped.dic
    assert force_text(dumped) == u'{"a": "12345\\u2026"}'


# pylint: disable=unused-argument,redefined-outer-name
def test_base_model(fake_conf, atreal_openads, collectivite_1, collectivite_1_guichet,
                    forwardfile_1):
    """Test for methods of class 'BaseModel' through instance of a ForwardFile."""

    assert forwardfile_1.get_verbose_name() == 'Forward File'
    assert forwardfile_1.get_verbose_name_plural() == 'Forward Files'

    assert forwardfile_1.get_class_name() == 'ForwardFile'
    assert forwardfile_1.get_class_name_plural() == 'ForwardFiles'

    assert forwardfile_1.get_class_name_dash_case() == 'forward-file'
    assert forwardfile_1.get_class_name_plural_dash_case() == 'forward-files'

    assert forwardfile_1.get_class_name_title() == 'Forward File'
    assert forwardfile_1.get_class_name_plural_title() == 'Forward Files'

    assert forwardfile_1.get_url_name('list', plural=True) == 'list-forward-files'

    assert forwardfile_1.get_absolute_url() == '/manage/atreal-openads/atreal/forward-file/%s' % forwardfile_1.id
    assert forwardfile_1.get_edit_url() == '/manage/atreal-openads/atreal/edit-forward-file/%s' % forwardfile_1.id
    assert forwardfile_1.get_delete_url() == '/manage/atreal-openads/atreal/delete-forward-file/%s' % forwardfile_1.id
    assert forwardfile_1.get_list_url() == '/manage/atreal-openads/atreal/forward-files'

    assert atreal_openads.get_class_name_plural() == 'AtrealOpenads'

    assert atreal_openads.get_url_name('view') == 'view-connector'

    params = atreal_openads.get_url_params(True)
    assert params['connector'] == 'atreal-openads'
    assert params['slug'] == atreal_openads.slug

    with pytest.raises(Exception) as exception:
        atreal_openads.get_list_url()
    assert force_text(exception.value) == u"AtrealOpenads:get_list_url() method should not be called"

    # TODO add more collectivite test cases

    with pytest.raises(Exception) as exception:
        collectivite_1_guichet.get_list_url()
    assert force_text(exception.value) == u"Guichet:get_list_url() method should not be called"
