#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of passerelle-atreal-openads - a Publik connector to openADS
#
# Copyright (C) 2019 Atreal
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""JSON schemas used by endpoints to validate input/ouput."""


# TODO add string limits (maxLength)


JSON_SCHEMA_FILE = {
    "description": "A file object",
    "$id": "#file",
    "type": "object",
    "properties": {
        "content": {"type": "string"},
        "content_type": {"type": ["string", "null"]},
        "filename": {"type": "string"}
    },
    "required": ["content", "filename"]
}

JSON_SCHEMA_FILE_B64 = {
    "description": "A file object encoded in base64",
    "$id": "#file",
    "type": "object",
    "properties": {
        "b64_content": {"type": "string"},
        "content_type": {"type": ["string", "null"]},
        "filename": {"type": "string"}
    },
    "required": ["b64_content", "filename"]
}

JSON_SCHEMA_DATE_FRENCH = {
    "type": "string",
    # pylint: disable=anomalous-backslash-in-string
    "pattern": "^(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/\d{4}$"  # noqa: W605
}

JSON_SCHEMA_CHECK_STATUS_OUT = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Response of an openADS 'connexion' test",
    "type": "object",
    "properties": {
        "response": {"type": "integer"}
    },
    "required": ["response"]
}

JSON_SCHEMA_CREATE_DOSSIER_IN = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Incoming request to create a 'dossier' in openADS.API",
    "definitions": {
        "refs-cadastrales": {
            "description": "The 3 items of a 'cadastrale' reference",
            "$id": "#refs-cadastrales",
            "type": "array",
            "items": {
                "type": "string"
            },
            "minItems": 3,
            "maxItems": 3
        },
        "file": JSON_SCHEMA_FILE,
        "file_plan_cadastral": {
            "description": "A 'plan cadastral' document file",
            "anyOf": [{"$ref": "#/definitions/file"}, {"type": "null"}]
        }
    },
    "type": "object",
    "properties": {
        "fields": {
            "type": "object",
            "properties": {
                "cerfa": {
                    "description": "A 'CERFA' PDF document file",
                    "type": "object",
                    "allOf": [{"$ref": "#/definitions/file"}]
                },
                "plan_cadastral_1": {"$ref": "#/definitions/file_plan_cadastral"},
                "plan_cadastral_2": {"$ref": "#/definitions/file_plan_cadastral"},
                "plan_cadastral_3": {"$ref": "#/definitions/file_plan_cadastral"},
                "plan_cadastral_4": {"$ref": "#/definitions/file_plan_cadastral"},
                "plan_cadastral_5": {"$ref": "#/definitions/file_plan_cadastral"},
                "terrain_numero_voie": {"type": "string"},
                "terrain_nom_voie": {"type": "string"},
                "terrain_code_postal": {"type": "string"},
                "terrain_localite": {"type": "string"},
                "terrain_lieu_dit": {"type": ["string", "null"]},
                "reference_cadastrale": {
                    "description": "A list of 'cadastrales' references",
                    "type": "array",
                    "items": {"$ref": "#/definitions/refs-cadastrales"}
                },
                "autres_parcelles": {
                    "type": "boolean"
                },
                "references_cadastrales": {
                    "description": "A list of 'cadastrales' references",
                    # conditionaly required and typed below
                },
                "proprietaire": {"enum": ["Oui", "Non"]},
                "qualite": {"type": "string"},
                "denomination": {},  # conditionaly required and typed below
                "raison_sociale": {},  # conditionaly required and typed below
                "nom": {"type": "string"},
                "prenom": {"type": "string"},
                "numero_voie": {"type": "string"},
                "nom_voie": {"type": "string"},
                "code_postal": {"type": "string"},
                "localite": {"type": "string"},
                "lieu_dit": {"type": ["string", "null"]},
                "mandataire_nom": {},  # conditionaly required and typed below
                "mandataire_prenom": {},  # conditionaly required and typed below
                "mandataire_numero_voie": {},  # conditionaly required and typed below
                "mandataire_nom_voie": {},  # conditionaly required and typed below
                "mandataire_code_postal": {},  # conditionaly required and typed below
                "mandataire_localite": {},  # conditionaly required and typed below
                "mandataire_lieu_dit": {}  # conditionaly required and typed below
            },

            # requirements
            "required": [
                "cerfa",
                "terrain_numero_voie",
                "terrain_nom_voie",
                "terrain_code_postal",
                "terrain_localite",
                "reference_cadastrale",
                #"proprietaire",
                "qualite",
                "nom",
                "prenom",
                "numero_voie",
                "nom_voie",
                "code_postal",
                "localite"
            ],

            # conditional requirements
            "allOf": [
                {
                    # qualite
                    "anyOf": [

                        # if qualite == "Un particulier"
                        # "denomination" and "raison_sociale" are not required
                        # and must be null
                        {
                            "properties": {
                                "qualite": {"const": "Un particulier"},
                                "denomination": {"type": "null"},
                                "raison_sociale": {"type": "null"}
                            }
                        },
                        # if qualite == "Une personne morale"
                        # "denomination" and "raison_sociale" are required
                        # and must be string
                        {
                            "properties": {
                                "qualite": {"const": "Une personne morale"},
                                "denomination": {"type": "string"},
                                "raison_sociale": {"type": "string"}
                            },
                            "required": ["denomination", "raison_sociale"]
                        }
                    ]
                },
                {
                    # proprietaire
                    "anyOf": [

                        # if proprietaire == "Oui"
                        # all the mandataire fields are not required
                        # and must be null
                        {
                            "properties": {
                                "proprietaire": {"const": "Oui"},
                                "mandataire_nom": {"type": "null"},
                                "mandataire_prenom": {"type": "null"},
                                "mandataire_numero_voie": {"type": "null"},
                                "mandataire_nom_voie": {"type": "null"},
                                "mandataire_code_postal": {"type": "null"},
                                "mandataire_localite": {"type": "null"},
                                "mandataire_lieu_dit": {"type": "null"}
                            }
                        },

                        # if proprietaire == "Non"
                        # all the mandataire fields are required (except the lieu_dit)
                        # and must be string
                        # and conditions are checked against mandataire_qualite
                        {
                            "properties": {
                                "proprietaire": {"const": "Non"},
                                "mandataire_nom": {"type": "string"},
                                "mandataire_prenom": {"type": "string"},
                                "mandataire_numero_voie": {"type": "string"},
                                "mandataire_nom_voie": {"type": "string"},
                                "mandataire_code_postal": {"type": "string"},
                                "mandataire_localite": {"type": "string"},
                                "mandataire_qualite": {"type": "string"},
                                "mandataire_lieu_dit": {"type": ["string", "null"]}
                            },
                            "required": [
                                "mandataire_nom",
                                "mandataire_prenom",
                                "mandataire_numero_voie",
                                "mandataire_nom_voie",
                                "mandataire_code_postal",
                                "mandataire_localite",
                                "mandataire_qualite"
                            ],

                            # mandataire_qualite
                            "anyOf": [

                                # if mandataire_qualite == "Un particulier"
                                # "mandataire_denomination" and "mandataire_raison_sociale"
                                # are not required and must be null
                                {
                                    "properties": {
                                        "mandataire_qualite": {"const": "Un particulier"},
                                        "mandataire_denomination": {"type": "null"},
                                        "mandataire_raison_sociale": {"type": "null"}
                                    }
                                },
                                # if mandataire_qualite == "Une personne morale"
                                # "mandataire_denomination" and "mandataire_raison_sociale"
                                # are required and must be string
                                {
                                    "properties": {
                                        "mandataire_qualite": {"const": "Une personne morale"},
                                        "mandataire_denomination": {"type": "string"},
                                        "mandataire_raison_sociale": {"type": "string"}
                                    },
                                    "required": [
                                        "mandataire_denomination",
                                        "mandataire_raison_sociale"
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    # autres_parcelles
                    "anyOf": [

                        # if autres_parcelles == False
                        # "references_cadastrales" is not required
                        # and must be null
                        {
                            "properties": {
                                "autres_parcelles": {"const": False},
                                "references_cadastrales": {"type": "null"}
                            }
                        },
                        # if autres_parcelles == True
                        # "references_cadastrales" is required
                        # and must be of type "array" of "refs-cadastrales"
                        {
                            "properties": {
                                "autres_parcelles": {"const": True},
                                "references_cadastrales": {
                                    "type": "array",
                                    "items": {"$ref": "#/definitions/refs-cadastrales"}
                                }
                            },
                            "required": ["autres_parcelles", "references_cadastrales"]
                        }
                    ]
                }
            ]
        }
    },
    "required": ["fields"]
}

JSON_SCHEMA_CREATE_DOSSIER_OUT = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Response of a 'dossier' creation in openADS.API",
    "type": "object",
    "properties": {
        "numero_dossier": {"type": "string"},
        "recepisse": JSON_SCHEMA_FILE_B64
    },
    "required": ["numero_dossier", "recepisse"]
}

JSON_SCHEMA_GET_DOSSIER_OUT = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Response of a 'dossier' creation in openADS.API",
    "type": "object",
    "properties": {
        "etat": {"type": "string"},
        "date_depot": JSON_SCHEMA_DATE_FRENCH,
        "date_decision": JSON_SCHEMA_DATE_FRENCH,
        "date_limite_instruction": JSON_SCHEMA_DATE_FRENCH,
        "decision": {"type": "string"}
    },
    "required": [
        "etat",
        "date_depot",
        "date_decision",
        "date_limite_instruction",
        "decision"
    ]
}

JSON_SCHEMA_FORWARDFILE = {
    "description": "A ForwardFile object (PDF document that must be forwarded to openADS)",
    "$id": "#forwardfile",
    "type": "object",
    "properties": {
        "id": {"type": "integer"},
        "numero_demande": {"type": "string"},
        "numero_dossier": {"type": "string"},
        "type_fichier": {"type": "string"},
        "file_hash": {"type": "string"},
        "orig_filename": {"type": "string"},
        "content_type": {"type": "string"},
        "upload_status": {"type": "string"},
        "upload_attempt": {"type": "integer"},
        "upload_msg": {"type": "string"},
        "content_size": {"type": "integer"},
        "last_update_datetime": {"type": "string", "format": "date-time"}
    },
    "required": [
        "id",
        "numero_demande",
        "numero_dossier",
        "type_fichier",
        "file_hash",
        "orig_filename",
        "content_type",
        "upload_status",
        "upload_attempt",
        "upload_msg",
        "content_size",
        "last_update_datetime"
    ]
}

JSON_SCHEMA_GET_FWD_FILES_OUT = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Response of a request about the forwarding (detailled) of user files to openADS",
    "type": "array",
    "items": {"$ref": "#/definitions/forwardfile"},
    "definitions": {
        "forwardfile": JSON_SCHEMA_FORWARDFILE
    }
}

JSON_SCHEMA_GET_FWD_FILE_STATUS = {
    "description": "The status of a ForwardFile",
    "$id": "#forwardfile-status",
    "type": "string",
    # pylint: disable=anomalous-backslash-in-string
    "pattern": "^\[\w+\] .+ => .+$"  # noqa: W605
}

JSON_SCHEMA_GET_FWD_FILES_STATUS_OUT = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Response of a request about the forwarding (summarized) of user files to openADS",
    "type": "object",
    "properties": {
        "all_forwarded": {"type": "boolean"},
        "pending": {"type": "array", "items": {"$ref": "#/definitions/forwardfile-status"}},
        "uploading": {"type": "array", "items": {"$ref": "#/definitions/forwardfile-status"}},
        "success": {"type": "array", "items": {"$ref": "#/definitions/forwardfile-status"}},
        "failed": {"type": "array", "items": {"$ref": "#/definitions/forwardfile-status"}}
    },
    "required": ["all_forwarded", "pending", "uploading", "success", "failed"],
    "definitions": {
        "forwardfile-status": JSON_SCHEMA_GET_FWD_FILE_STATUS
    }
}

JSON_SCHEMA_GET_COURRIER_OUT = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "title": "Response of a 'courrier' from an openADS 'dossier'",
    "type": "object",
    "properties": {
        "courrier": JSON_SCHEMA_FILE_B64
    },
    "required": ["courrier"]
}
