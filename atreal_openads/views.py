#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of passerelle-atreal-openads - a Publik connector to openADS
#
# Copyright (C) 2019 Atreal
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Views for the models."""

from django.urls import reverse_lazy

from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from passerelle.views import GenericConnectorView

from .models import ForwardFile, Collectivite, Guichet, AtrealOpenads
from .forms import ForwardFileForm, CollectiviteForm, GuichetForm


def get_connecteur_from_request(view, key='connecteur'):
    """Return the 'connecteur' object from the view object."""
    if not hasattr(view, 'connecteur') or not view.connecteur and view.request:
        connecteur_slug = view.request.resolver_match.kwargs.get(key, None)
        if connecteur_slug:
            view.connecteur = AtrealOpenads.objects.get(slug=connecteur_slug)
    return view.connecteur if hasattr(view, 'connecteur') else None


def get_collectivite_from_request(view, key='collectivite'):
    """Return the 'collectivite' object from the view object."""
    if not hasattr(view, 'collectivite') or not view.collectivite and view.request:
        collectivite_id = view.request.resolver_match.kwargs.get(key, None)
        if collectivite_id:
            # pylint: disable=no-member
            view.collectivite = Collectivite.objects.get(id=collectivite_id)
    return view.collectivite if hasattr(view, 'collectivite') else None


# pylint: disable=too-many-ancestors
class ForwardFileView(DetailView):
    """View to display a ForwardFile."""

    model = ForwardFile
    template_name = 'atreal_openads/manage/forwardfile_view.html'

    def get_context_data(self, **kwargs):
        context = super(ForwardFileView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        return context


# pylint: disable=too-many-ancestors
class ForwardFileListView(ListView):
    """View to display a list of ForwardFiles."""

    model = ForwardFile
    template_name = 'atreal_openads/manage/forwardfile_list.html'
    paginate_by = 50
    ordering = 'id'

    def get_queryset(self):
        qset = None

        collectivite = get_collectivite_from_request(self)
        if collectivite:
            qset = super(ForwardFileListView, self).get_queryset().filter(
                connecteur=get_connecteur_from_request(self),
                collectivite=collectivite)
        else:
            qset = super(ForwardFileListView, self).get_queryset().filter(
                connecteur=get_connecteur_from_request(self))

        order_by = None
        order_by_param = self.request.GET.get('order-by', None)
        if order_by_param:
            fields_names = [f.name for f in self.model.get_fields()]
            order_by_field = order_by_param[1:] if order_by_param[0] == '-' else order_by_param
            if order_by_field in fields_names:
                order_by = order_by_param

        return qset.order_by(order_by) if order_by else qset  # qset.order_by()

    def get_context_data(self, **kwargs):
        context = super(ForwardFileListView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['collectivite'] = get_collectivite_from_request(self)
        return context


# pylint: disable=too-many-ancestors
class ForwardFileUpdateView(UpdateView):
    """View to edit a ForwardFile."""

    model = ForwardFile
    form_class = ForwardFileForm
    template_name = 'atreal_openads/manage/forwardfile_form.html'

    def get_context_data(self, **kwargs):
        context = super(ForwardFileUpdateView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['collectivite'] = get_collectivite_from_request(self)
        return context

    def get_success_url(self):
        back_to = self.request.GET.get('back-to')
        if back_to == 'list-forward-files':
            return reverse_lazy('list-forward-files', kwargs={
                'connecteur': get_connecteur_from_request(self).slug
            })
        elif back_to == 'col-list-forward-files':
            obj = self.get_object()
            if obj.collectivite:
                return reverse_lazy('col-list-forward-files', kwargs={
                    'connecteur': get_connecteur_from_request(self).slug,
                    'collectivite': obj.collectivite.id
                })
        return self.get_object().get_absolute_url()


# pylint: disable=too-many-ancestors
class ForwardFileDeleteView(DeleteView):
    """View to delete a ForwardFile."""

    model = ForwardFile
    form_class = ForwardFileForm
    template_name = 'atreal_openads/manage/forwardfile_form.html'

    def get_context_data(self, **kwargs):
        context = super(ForwardFileDeleteView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['collectivite'] = get_collectivite_from_request(self)
        return context

    def get_success_url(self):
        back_to = self.request.GET.get('back-to')
        if back_to == 'list-forward-files':
            return reverse_lazy('list-forward-files', kwargs={
                'connecteur': get_connecteur_from_request(self).slug
            })
        elif back_to == 'col-list-forward-files':
            obj = self.get_object()
            if obj.collectivite:
                return reverse_lazy('col-list-forward-files', kwargs={
                    'connecteur': get_connecteur_from_request(self).slug,
                    'collectivite': obj.collectivite.id
                })
        return reverse_lazy('view-connector', kwargs={
            'connector': 'atreal-openads',
            'slug': get_connecteur_from_request(self).slug
        })


# pylint: disable=too-many-ancestors
class CollectiviteView(DetailView):
    """View to display a Collectivite."""

    model = Collectivite
    template_name = 'atreal_openads/manage/collectivite_view.html'

    def get_context_data(self, **kwargs):
        context = super(CollectiviteView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['guichet_add_url'] = reverse_lazy('create-guichet', kwargs={
            'connecteur': context['connecteur'].slug,
            'collectivite': self.get_object().id})
        context['forward_files_list_url'] = reverse_lazy('col-list-forward-files', kwargs={
            'connecteur': context['connecteur'].slug,
            'collectivite': self.get_object().id})
        return context


# pylint: disable=too-many-ancestors
class CollectiviteListView(ListView):
    """View to display a list of Collectivites."""

    model = Collectivite
    template_name = 'atreal_openads/manage/collectivite_list.html'
    paginate_by = 50
    ordering = 'id'

    def get_queryset(self):
        qset = super(CollectiviteListView, self).get_queryset().filter(
            connecteur=get_connecteur_from_request(self))

        order_by = None
        order_by_param = self.request.GET.get('order-by', None)
        if order_by_param:
            fields_names = [f.name for f in self.model.get_fields()]
            order_by_field = order_by_param[1:] if order_by_param[0] == '-' else order_by_param
            if order_by_field in fields_names:
                order_by = order_by_param

        return qset.order_by(order_by) if order_by else qset  # qset.order_by()

    def get_context_data(self, **kwargs):
        context = super(CollectiviteListView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['collectivite_add_url'] = reverse_lazy('create-collectivite', kwargs={
            'connecteur': context['connecteur'].slug})
        return context


# pylint: disable=too-many-ancestors
class CollectiviteCreateView(CreateView):
    """View to create a Collectivite."""

    model = Collectivite
    form_class = CollectiviteForm
    template_name = 'atreal_openads/manage/collectivite_form.html'

    def get_context_data(self, **kwargs):
        context = super(CollectiviteCreateView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        return context

    def get_form_kwargs(self):
        kwargs = super(CollectiviteCreateView, self).get_form_kwargs()
        kwargs['connecteur'] = get_connecteur_from_request(self)
        return kwargs

    def get_success_url(self):
        if self.request.GET.get('back-to') == 'list-collectivites':
            return reverse_lazy('list-collectivites', kwargs={
                'connecteur': get_connecteur_from_request(self).slug
            })
        return reverse_lazy('view-connector', kwargs={
            'connector': 'atreal-openads',
            'slug': get_connecteur_from_request(self).slug
        })


# pylint: disable=too-many-ancestors
class CollectiviteUpdateView(UpdateView):
    """View to edit a Collectivite."""

    model = Collectivite
    form_class = CollectiviteForm
    template_name = 'atreal_openads/manage/collectivite_form.html'

    def get_context_data(self, **kwargs):
        context = super(CollectiviteUpdateView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        return context

    def get_success_url(self):
        if self.request.GET.get('back-to') == 'list-collectivites':
            return reverse_lazy('list-collectivites', kwargs={
                'connecteur': get_connecteur_from_request(self).slug
            })
        return self.get_object().get_absolute_url()


# pylint: disable=too-many-ancestors
class CollectiviteDeleteView(DeleteView):
    """View to delete a Collectivite."""

    model = Collectivite
    form_class = CollectiviteForm
    template_name = 'atreal_openads/manage/collectivite_form.html'

    def get_context_data(self, **kwargs):
        context = super(CollectiviteDeleteView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        return context

    def get_success_url(self):
        if self.request.GET.get('back-to') == 'list-collectivites':
            return reverse_lazy('list-collectivites', kwargs={
                'connecteur': get_connecteur_from_request(self).slug
            })
        return reverse_lazy('view-connector', kwargs={
            'connector': 'atreal-openads',
            'slug': get_connecteur_from_request(self).slug
        })


# pylint: disable=too-many-ancestors
class GuichetView(DetailView):
    """View to display a Guichet."""

    model = Guichet
    template_name = 'atreal_openads/manage/guichet_view.html'

    def get_context_data(self, **kwargs):
        context = super(GuichetView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['collectivite'] = get_collectivite_from_request(self)
        return context


# pylint: disable=too-many-ancestors
class GuichetCreateView(CreateView):
    """View to create a Guichet."""

    model = Guichet
    form_class = GuichetForm
    template_name = 'atreal_openads/manage/guichet_form.html'

    def get_form_kwargs(self):
        kwargs = super(GuichetCreateView, self).get_form_kwargs()
        kwargs['collectivite'] = get_collectivite_from_request(self)
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(GuichetCreateView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['collectivite'] = get_collectivite_from_request(self)
        return context

    def get_success_url(self):
        return reverse_lazy('view-collectivite', kwargs={
            'connecteur': get_connecteur_from_request(self).slug,
            'pk': get_collectivite_from_request(self).id
        })


# pylint: disable=too-many-ancestors
class GuichetUpdateView(UpdateView):
    """View to edit a Guichet."""

    model = Guichet
    form_class = GuichetForm
    template_name = 'atreal_openads/manage/guichet_form.html'

    def get_context_data(self, **kwargs):
        context = super(GuichetUpdateView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['collectivite'] = get_collectivite_from_request(self)
        return context


# pylint: disable=too-many-ancestors
class GuichetDeleteView(DeleteView):
    """View to delete a Guichet."""

    model = Guichet
    form_class = GuichetForm
    template_name = 'atreal_openads/manage/guichet_form.html'

    def get_context_data(self, **kwargs):
        context = super(GuichetDeleteView, self).get_context_data(**kwargs)
        context['connecteur'] = get_connecteur_from_request(self)
        context['collectivite'] = get_collectivite_from_request(self)
        return context

    def get_success_url(self):
        return reverse_lazy('view-collectivite', kwargs={
            'connecteur': get_connecteur_from_request(self).slug,
            'pk': get_collectivite_from_request(self).id
        })


# pylint: disable=too-many-ancestors
class AtrealOpenadsView(GenericConnectorView):
    """View to display a connector AtrealOpenads."""

    model = AtrealOpenads
    template_name = 'atreal_openads/manage/connector_view.html'

    def get_context_data(self, slug=None, **kwargs):
        context = super(AtrealOpenadsView, self).get_context_data(slug=slug, **kwargs)
        context['collectivite_fields'] = Collectivite.get_fields()
        context['collectivite_add_url'] = reverse_lazy('create-collectivite', kwargs={
            'connecteur': self.get_object().slug})
        return context
