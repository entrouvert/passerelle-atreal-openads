#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of passerelle-atreal-openads - a Publik connector to openADS
#
# Copyright (C) 2019 Atreal
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Forms for each of the Models."""

from django.forms import ModelForm

from .models import ForwardFile, Collectivite, Guichet


class ForwardFileForm(ModelForm):
    """Form for the ForwardFile model."""

    class Meta:
        # pylint: disable=too-few-public-methods,no-init,old-style-class,missing-docstring
        model = ForwardFile
        exclude = ['connecteur', 'size', 'file_hash']

    def __init__(self, *args, **kwargs):

        connecteur = kwargs.pop('connecteur', None)
        collectivite = kwargs.pop('collectivite', None)

        super(ForwardFileForm, self).__init__(*args, **kwargs)

        if ((not hasattr(self.instance, 'connecteur') or not self.instance.connecteur)
                and connecteur):
            self.instance.connecteur = connecteur
        if ((not hasattr(self.instance, 'collectivite') or not self.instance.collectivite)
                and collectivite):
            self.instance.collectivite = collectivite

        # only allow to select a 'collectivite' that belongs to the connecteur
        if hasattr(self.instance, 'connecteur') and self.instance.connecteur:
            # pylint: disable=no-member
            self.fields['collectivite'].queryset = Collectivite.objects.filter(
                connecteur=self.instance.connecteur)

    # TODO if the status is 'uploading' make everything read-only


class CollectiviteForm(ModelForm):
    """Form for the Collectivite model."""

    class Meta:
        # pylint: disable=too-few-public-methods,no-init,old-style-class,missing-docstring
        model = Collectivite
        exclude = ['connecteur']

    def __init__(self, *args, **kwargs):
        connecteur = kwargs.pop('connecteur', None)
        super(CollectiviteForm, self).__init__(*args, **kwargs)
        if ((not hasattr(self.instance, 'connecteur') or not self.instance.connecteur)
                and connecteur):
            self.instance.connecteur = connecteur


class GuichetForm(ModelForm):
    """Form for the Guichet model."""

    class Meta:
        # pylint: disable=too-few-public-methods,no-init,old-style-class,missing-docstring
        model = Guichet
        exclude = ['collectivite']

    def __init__(self, *args, **kwargs):
        collectivite = kwargs.pop('collectivite', None)
        super(GuichetForm, self).__init__(*args, **kwargs)
        if ((not hasattr(self.instance, 'collectivite') or not self.instance.collectivite)
                and collectivite):
            self.instance.collectivite = collectivite
