#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This file is part of passerelle-atreal-openads - a Publik connector to openADS
#
# Copyright (C) 2019 Atreal
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""The urls for this connector module and its entities views."""

import re

from django.conf.urls import url

from .views import (
    AtrealOpenadsView,
    ForwardFileView,
    ForwardFileListView,
    ForwardFileUpdateView,
    ForwardFileDeleteView,
    CollectiviteView,
    CollectiviteListView,
    CollectiviteCreateView,
    CollectiviteUpdateView,
    CollectiviteDeleteView,
    GuichetView,
    GuichetCreateView,
    GuichetUpdateView,
    GuichetDeleteView
)


# pylint: disable=invalid-name
urlpatterns = [
    url(r'^(?P<slug>[\w,-]+)/$', AtrealOpenadsView.as_view(), name='view-connector')
]

# pylint: disable=invalid-name
management_urlpatterns = []

for view in [
        ForwardFileView,
        ForwardFileListView,
        ForwardFileUpdateView,
        ForwardFileDeleteView,
        CollectiviteView,
        CollectiviteListView,
        CollectiviteCreateView,
        CollectiviteUpdateView,
        CollectiviteDeleteView,
        GuichetView,
        GuichetCreateView,
        GuichetUpdateView,
        GuichetDeleteView]:
    view_class_name = str(view.__name__)
    m = re.search(r'^.*(Create|Update|Delete|List)View$', view_class_name)
    if m:
        view_action = m.group(1).lower()
    else:
        view_action = 'view'

    # no prefix for action 'view'
    url_prefix = view_action.replace('update', 'edit') + '-'

    regex_base = r'^(?P<connecteur>[\w,-]+)/'
    # pylint: disable=anomalous-backslash-in-string
    regex_pkey = '/(?P<pk>[\w,-]+)'  # noqa: W605

    url_name = url_prefix + view.model.get_class_name_dash_case()
    regex_url = '%s%s' % (url_prefix if view_action != 'view' else '',
                          view.model.get_class_name_dash_case())

    # no primary key for action 'create' and 'list'
    if view_action in ['create', 'list']:
        regex_pkey = ''

    # plural form of the url for action 'list' and no prefix
    if view_action == 'list':
        url_name = url_prefix + view.model.get_class_name_plural_dash_case()
        regex_url = view.model.get_class_name_plural_dash_case()

    # for 'guichet' prefix the regex by the collectivite
    if view.model.get_class_name() == 'Guichet':
        regex_base += 'collectivite/(?P<collectivite>[\w,-]+)/'  # noqa: W605

    # build the regex
    regex = regex_base + regex_url + regex_pkey + '$'

    # add the url pattern to the management list
    management_urlpatterns += [url(regex, view.as_view(), name=url_name)]

# add the ForwardFile 'list' url patterns for Collectivite
ff_list_regex_url = ForwardFileListView.model.get_class_name_plural_dash_case()
management_urlpatterns += [
    url(
        r'^(?P<connecteur>[\w,-]+)/collectivite/(?P<collectivite>[\w,-]+)/%s$' % ff_list_regex_url,
        ForwardFileListView.as_view(),
        name='col-list-' + ff_list_regex_url
    )
]
