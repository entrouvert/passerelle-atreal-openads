/**
 * This file is part of passerelle-atreal-openads - a Publik connector to openADS
 *
 * Copyright (C) 2019 Atreal
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

@Library('eo-jenkins-lib@main') import eo.Utils

pipeline {
    agent any
    stages {
        stage('Unit Tests') {
            steps {
                sh 'tox -rv'
            }
            post {
                always {
                    script {
                        utils = new Utils()
                        utils.publish_coverage('coverage.xml')
                        utils.publish_coverage_native('index.html')
                        utils.publish_pylint('pylint.out')
                    }
                    mergeJunitResults()
                }
            }
        }
        stage('Packaging') {
            steps {
                script {
                    env.SHORT_JOB_NAME=sh(
                        returnStdout: true,
                        // given JOB_NAME=gitea/project/PR-46, returns project
                        // given JOB_NAME=project/main, returns project
                        script: '''
                            echo "${JOB_NAME}" | sed "s/gitea\\///" | awk -F/ '{print $1}'
                        '''
                    ).trim()
                    if (env.GIT_BRANCH == 'main' || env.GIT_BRANCH == 'origin/main') {
                        sh "sudo -H -u eobuilder /usr/local/bin/eobuilder ${SHORT_JOB_NAME}"
                    } else if (env.GIT_BRANCH.startsWith('hotfix/')) {
                        sh "sudo -H -u eobuilder /usr/local/bin/eobuilder --branch ${env.GIT_BRANCH} --hotfix ${SHORT_JOB_NAME}"
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                utils = new Utils()
                utils.mail_notify(currentBuild, env, 'ci+jenkins-passerelle-atreal-openads@entrouvert.org')
            }
        }
        success {
            cleanWs()
        }
    }
}
